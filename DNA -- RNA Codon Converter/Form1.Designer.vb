﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCodonConverter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.radDnaCodon1 = New System.Windows.Forms.RadioButton
        Me.txtDnaCodon1 = New System.Windows.Forms.TextBox
        Me.txtDnaCodon2 = New System.Windows.Forms.TextBox
        Me.radDnaCodon2 = New System.Windows.Forms.RadioButton
        Me.txtMrnaCodon = New System.Windows.Forms.TextBox
        Me.radMrnaCodon = New System.Windows.Forms.RadioButton
        Me.grpMrnaCodonDetermine = New System.Windows.Forms.GroupBox
        Me.radDetermineDnaCodon2 = New System.Windows.Forms.RadioButton
        Me.radDetermineDnaCodon1 = New System.Windows.Forms.RadioButton
        Me.txtTrnaCodon = New System.Windows.Forms.TextBox
        Me.radTrnaCodon = New System.Windows.Forms.RadioButton
        Me.btnFindCodons = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnAbout = New System.Windows.Forms.Button
        Me.lblAminoAcid = New System.Windows.Forms.Label
        Me.txtAminoAcid = New System.Windows.Forms.TextBox
        Me.webUpdate = New System.Windows.Forms.WebBrowser
        Me.webUpdateLink = New System.Windows.Forms.WebBrowser
        Me.grpMrnaCodonDetermine.SuspendLayout()
        Me.SuspendLayout()
        '
        'radDnaCodon1
        '
        Me.radDnaCodon1.AutoSize = True
        Me.radDnaCodon1.Checked = True
        Me.radDnaCodon1.Location = New System.Drawing.Point(55, 13)
        Me.radDnaCodon1.Name = "radDnaCodon1"
        Me.radDnaCodon1.Size = New System.Drawing.Size(101, 17)
        Me.radDnaCodon1.TabIndex = 0
        Me.radDnaCodon1.TabStop = True
        Me.radDnaCodon1.Text = "DNA Codon #1:"
        Me.radDnaCodon1.UseVisualStyleBackColor = True
        '
        'txtDnaCodon1
        '
        Me.txtDnaCodon1.Location = New System.Drawing.Point(170, 12)
        Me.txtDnaCodon1.Name = "txtDnaCodon1"
        Me.txtDnaCodon1.Size = New System.Drawing.Size(100, 20)
        Me.txtDnaCodon1.TabIndex = 1
        '
        'txtDnaCodon2
        '
        Me.txtDnaCodon2.Location = New System.Drawing.Point(170, 38)
        Me.txtDnaCodon2.Name = "txtDnaCodon2"
        Me.txtDnaCodon2.Size = New System.Drawing.Size(100, 20)
        Me.txtDnaCodon2.TabIndex = 3
        '
        'radDnaCodon2
        '
        Me.radDnaCodon2.AutoSize = True
        Me.radDnaCodon2.Location = New System.Drawing.Point(55, 39)
        Me.radDnaCodon2.Name = "radDnaCodon2"
        Me.radDnaCodon2.Size = New System.Drawing.Size(101, 17)
        Me.radDnaCodon2.TabIndex = 2
        Me.radDnaCodon2.TabStop = True
        Me.radDnaCodon2.Text = "DNA Codon #2:"
        Me.radDnaCodon2.UseVisualStyleBackColor = True
        '
        'txtMrnaCodon
        '
        Me.txtMrnaCodon.Location = New System.Drawing.Point(170, 64)
        Me.txtMrnaCodon.Name = "txtMrnaCodon"
        Me.txtMrnaCodon.Size = New System.Drawing.Size(100, 20)
        Me.txtMrnaCodon.TabIndex = 5
        '
        'radMrnaCodon
        '
        Me.radMrnaCodon.AutoSize = True
        Me.radMrnaCodon.Location = New System.Drawing.Point(55, 65)
        Me.radMrnaCodon.Name = "radMrnaCodon"
        Me.radMrnaCodon.Size = New System.Drawing.Size(93, 17)
        Me.radMrnaCodon.TabIndex = 4
        Me.radMrnaCodon.TabStop = True
        Me.radMrnaCodon.Text = "mRNA Codon:"
        Me.radMrnaCodon.UseVisualStyleBackColor = True
        '
        'grpMrnaCodonDetermine
        '
        Me.grpMrnaCodonDetermine.Controls.Add(Me.radDetermineDnaCodon2)
        Me.grpMrnaCodonDetermine.Controls.Add(Me.radDetermineDnaCodon1)
        Me.grpMrnaCodonDetermine.Location = New System.Drawing.Point(55, 90)
        Me.grpMrnaCodonDetermine.Name = "grpMrnaCodonDetermine"
        Me.grpMrnaCodonDetermine.Size = New System.Drawing.Size(215, 49)
        Me.grpMrnaCodonDetermine.TabIndex = 6
        Me.grpMrnaCodonDetermine.TabStop = False
        Me.grpMrnaCodonDetermine.Text = "Determine mRNA Codon from..."
        '
        'radDetermineDnaCodon2
        '
        Me.radDetermineDnaCodon2.AutoSize = True
        Me.radDetermineDnaCodon2.Location = New System.Drawing.Point(110, 16)
        Me.radDetermineDnaCodon2.Name = "radDetermineDnaCodon2"
        Me.radDetermineDnaCodon2.Size = New System.Drawing.Size(98, 17)
        Me.radDetermineDnaCodon2.TabIndex = 8
        Me.radDetermineDnaCodon2.TabStop = True
        Me.radDetermineDnaCodon2.Text = "DNA Codon #2"
        Me.radDetermineDnaCodon2.UseVisualStyleBackColor = True
        '
        'radDetermineDnaCodon1
        '
        Me.radDetermineDnaCodon1.AutoSize = True
        Me.radDetermineDnaCodon1.Checked = True
        Me.radDetermineDnaCodon1.Location = New System.Drawing.Point(6, 16)
        Me.radDetermineDnaCodon1.Name = "radDetermineDnaCodon1"
        Me.radDetermineDnaCodon1.Size = New System.Drawing.Size(98, 17)
        Me.radDetermineDnaCodon1.TabIndex = 7
        Me.radDetermineDnaCodon1.TabStop = True
        Me.radDetermineDnaCodon1.Text = "DNA Codon #1"
        Me.radDetermineDnaCodon1.UseVisualStyleBackColor = True
        '
        'txtTrnaCodon
        '
        Me.txtTrnaCodon.Location = New System.Drawing.Point(170, 145)
        Me.txtTrnaCodon.Name = "txtTrnaCodon"
        Me.txtTrnaCodon.Size = New System.Drawing.Size(100, 20)
        Me.txtTrnaCodon.TabIndex = 8
        '
        'radTrnaCodon
        '
        Me.radTrnaCodon.AutoSize = True
        Me.radTrnaCodon.Location = New System.Drawing.Point(55, 146)
        Me.radTrnaCodon.Name = "radTrnaCodon"
        Me.radTrnaCodon.Size = New System.Drawing.Size(88, 17)
        Me.radTrnaCodon.TabIndex = 7
        Me.radTrnaCodon.TabStop = True
        Me.radTrnaCodon.Text = "tRNA Codon:"
        Me.radTrnaCodon.UseVisualStyleBackColor = True
        '
        'btnFindCodons
        '
        Me.btnFindCodons.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnFindCodons.Location = New System.Drawing.Point(55, 197)
        Me.btnFindCodons.Name = "btnFindCodons"
        Me.btnFindCodons.Size = New System.Drawing.Size(215, 27)
        Me.btnFindCodons.TabIndex = 9
        Me.btnFindCodons.Text = "&Find Codons"
        Me.btnFindCodons.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClear.Location = New System.Drawing.Point(55, 230)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(215, 27)
        Me.btnClear.TabIndex = 10
        Me.btnClear.Text = "&Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnAbout
        '
        Me.btnAbout.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnAbout.Location = New System.Drawing.Point(55, 263)
        Me.btnAbout.Name = "btnAbout"
        Me.btnAbout.Size = New System.Drawing.Size(215, 27)
        Me.btnAbout.TabIndex = 11
        Me.btnAbout.Text = "&About"
        Me.btnAbout.UseVisualStyleBackColor = True
        '
        'lblAminoAcid
        '
        Me.lblAminoAcid.AutoSize = True
        Me.lblAminoAcid.Location = New System.Drawing.Point(52, 174)
        Me.lblAminoAcid.Name = "lblAminoAcid"
        Me.lblAminoAcid.Size = New System.Drawing.Size(63, 13)
        Me.lblAminoAcid.TabIndex = 12
        Me.lblAminoAcid.Text = "Amino Acid:"
        '
        'txtAminoAcid
        '
        Me.txtAminoAcid.Location = New System.Drawing.Point(121, 171)
        Me.txtAminoAcid.Name = "txtAminoAcid"
        Me.txtAminoAcid.ReadOnly = True
        Me.txtAminoAcid.Size = New System.Drawing.Size(149, 20)
        Me.txtAminoAcid.TabIndex = 13
        '
        'webUpdate
        '
        Me.webUpdate.Location = New System.Drawing.Point(1, 280)
        Me.webUpdate.MinimumSize = New System.Drawing.Size(20, 20)
        Me.webUpdate.Name = "webUpdate"
        Me.webUpdate.Size = New System.Drawing.Size(20, 20)
        Me.webUpdate.TabIndex = 14
        Me.webUpdate.Url = New System.Uri("http://www.programmersworld.elementfx.com/projects/DNAtoRNACodonConverter/UpdateC" & _
                "heck.php?version=1.2.0&versionStatus=none", System.UriKind.Absolute)
        Me.webUpdate.Visible = False
        '
        'webUpdateLink
        '
        Me.webUpdateLink.Location = New System.Drawing.Point(1, 254)
        Me.webUpdateLink.MinimumSize = New System.Drawing.Size(20, 20)
        Me.webUpdateLink.Name = "webUpdateLink"
        Me.webUpdateLink.Size = New System.Drawing.Size(20, 20)
        Me.webUpdateLink.TabIndex = 15
        Me.webUpdateLink.Url = New System.Uri("http://www.programmersworld.elementfx.com/projects/DNAtoRNACodonConverter/UpdateC" & _
                "heck.php?version=&versionStatus=old", System.UriKind.Absolute)
        Me.webUpdateLink.Visible = False
        '
        'frmCodonConverter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(322, 303)
        Me.Controls.Add(Me.webUpdateLink)
        Me.Controls.Add(Me.webUpdate)
        Me.Controls.Add(Me.txtAminoAcid)
        Me.Controls.Add(Me.lblAminoAcid)
        Me.Controls.Add(Me.btnAbout)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnFindCodons)
        Me.Controls.Add(Me.txtTrnaCodon)
        Me.Controls.Add(Me.radTrnaCodon)
        Me.Controls.Add(Me.grpMrnaCodonDetermine)
        Me.Controls.Add(Me.txtMrnaCodon)
        Me.Controls.Add(Me.radMrnaCodon)
        Me.Controls.Add(Me.txtDnaCodon2)
        Me.Controls.Add(Me.radDnaCodon2)
        Me.Controls.Add(Me.txtDnaCodon1)
        Me.Controls.Add(Me.radDnaCodon1)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(330, 330)
        Me.MinimumSize = New System.Drawing.Size(330, 330)
        Me.Name = "frmCodonConverter"
        Me.Text = "DNA/RNA Codon Converter v1.2.0"
        Me.grpMrnaCodonDetermine.ResumeLayout(False)
        Me.grpMrnaCodonDetermine.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents radDnaCodon1 As System.Windows.Forms.RadioButton
    Friend WithEvents txtDnaCodon1 As System.Windows.Forms.TextBox
    Friend WithEvents txtDnaCodon2 As System.Windows.Forms.TextBox
    Friend WithEvents radDnaCodon2 As System.Windows.Forms.RadioButton
    Friend WithEvents txtMrnaCodon As System.Windows.Forms.TextBox
    Friend WithEvents radMrnaCodon As System.Windows.Forms.RadioButton
    Friend WithEvents grpMrnaCodonDetermine As System.Windows.Forms.GroupBox
    Friend WithEvents radDetermineDnaCodon2 As System.Windows.Forms.RadioButton
    Friend WithEvents radDetermineDnaCodon1 As System.Windows.Forms.RadioButton
    Friend WithEvents txtTrnaCodon As System.Windows.Forms.TextBox
    Friend WithEvents radTrnaCodon As System.Windows.Forms.RadioButton
    Friend WithEvents btnFindCodons As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnAbout As System.Windows.Forms.Button
    Friend WithEvents lblAminoAcid As System.Windows.Forms.Label
    Friend WithEvents txtAminoAcid As System.Windows.Forms.TextBox
    Private WithEvents webUpdate As System.Windows.Forms.WebBrowser
    Private WithEvents webUpdateLink As System.Windows.Forms.WebBrowser

End Class
