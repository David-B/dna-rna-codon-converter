# DNA/RNA Codon Converter #

This program converts DNA/RNA codon information from one form to another and determines what the resulting amino acid would be. Written in Visual Basic, this program is no longer in active development. Derivative works are permitted provided that the original source be credited (MIT License).

**This program is available in the form of an Android app for devices running Android Ice Cream Sandwich or newer on the [DNA/RNA Codon Converter (Android) repository page](https://bitbucket.org/David-B/dna-rna-codon-converter-android).**

The build dates for each of the four binaries are as follows:

* v1.0.0 - April 3, 2013
* v1.0.1 - May 3, 2013
* v1.1.0 - June 3, 2013
* v1.2.0 - November 3, 2013

**This program is not perfect, and I am aware of at least several issues that are present in the final release (v1.2.0). I make this code publicly available with the hope that it will be beneficial to someone, whether they are learning to program or need some help with understanding how DNA/RNA codons work. Feel free to make changes and redistribute.**

## I don't want the code. Where can I get just the program? ##

Downloads for each version of the DNA/RNA Codon Converter are available on the [repository's downloads page](https://bitbucket.org/David-B/dna-rna-codon-converter/downloads). The recommended version to try is DNA/RNA Codon Converter v1.2.0 because it is the last version of the program that I released.

## Redistribution License (MIT) ##

The MIT License (MIT)

Copyright (c) 2015-2016 David Berdik

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.