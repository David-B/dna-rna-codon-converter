﻿Public Class frmUpdate
    'This will redirect the browser to the download page for the executable download,
    'prompting the user for downloading.
    Private Sub btnDownloadExe_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownloadExe.Click
        webDownloadElement.Navigate("http://www.programmersworld.elementfx.com/projects/DNAtoRNACodonConverter/LatestExeDownload.html")
    End Sub

    'This will redirect the browser to the download page for the zip download, prompting
    'the user for downloading.
    Private Sub btnDownloadZip_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadZip.Click
        webDownloadElement.Navigate("http://www.programmersworld.elementfx.com/projects/DNAtoRNACodonConverter/LatestZipDownload.html")
    End Sub

    'This will close the box if the user wishes to ignore the alert.
    Private Sub btnIgnore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIgnore.Click
        Me.Close()
    End Sub
End Class