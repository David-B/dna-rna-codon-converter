﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUpdate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblOutdated = New System.Windows.Forms.Label
        Me.grpExecutableDownload = New System.Windows.Forms.GroupBox
        Me.btnDownloadExe = New System.Windows.Forms.Button
        Me.lblExecutableDescription = New System.Windows.Forms.Label
        Me.grpZipDownload = New System.Windows.Forms.GroupBox
        Me.lblZipDescription = New System.Windows.Forms.Label
        Me.btnDownloadZip = New System.Windows.Forms.Button
        Me.btnIgnore = New System.Windows.Forms.Button
        Me.webDownloadElement = New System.Windows.Forms.WebBrowser
        Me.grpExecutableDownload.SuspendLayout()
        Me.grpZipDownload.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblOutdated
        '
        Me.lblOutdated.Location = New System.Drawing.Point(12, 24)
        Me.lblOutdated.Name = "lblOutdated"
        Me.lblOutdated.Size = New System.Drawing.Size(346, 47)
        Me.lblOutdated.TabIndex = 0
        Me.lblOutdated.Text = "Your version of ""DNA/RNA Codon Converter"" is outdated. Please download the latest" & _
            " version of ""DNA/RNA Codon Converter"" by choosing one of the download options pr" & _
            "ovided below."
        Me.lblOutdated.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grpExecutableDownload
        '
        Me.grpExecutableDownload.Controls.Add(Me.webDownloadElement)
        Me.grpExecutableDownload.Controls.Add(Me.lblExecutableDescription)
        Me.grpExecutableDownload.Controls.Add(Me.btnDownloadExe)
        Me.grpExecutableDownload.Location = New System.Drawing.Point(15, 83)
        Me.grpExecutableDownload.Name = "grpExecutableDownload"
        Me.grpExecutableDownload.Size = New System.Drawing.Size(343, 100)
        Me.grpExecutableDownload.TabIndex = 1
        Me.grpExecutableDownload.TabStop = False
        '
        'btnDownloadExe
        '
        Me.btnDownloadExe.Location = New System.Drawing.Point(13, 15)
        Me.btnDownloadExe.Name = "btnDownloadExe"
        Me.btnDownloadExe.Size = New System.Drawing.Size(319, 23)
        Me.btnDownloadExe.TabIndex = 0
        Me.btnDownloadExe.Text = "Download As Executable (Binary) File"
        Me.btnDownloadExe.UseVisualStyleBackColor = True
        '
        'lblExecutableDescription
        '
        Me.lblExecutableDescription.Location = New System.Drawing.Point(10, 41)
        Me.lblExecutableDescription.Name = "lblExecutableDescription"
        Me.lblExecutableDescription.Size = New System.Drawing.Size(322, 50)
        Me.lblExecutableDescription.TabIndex = 1
        Me.lblExecutableDescription.Text = "If possible, this is the download option that you should use. This will download " & _
            "the program directly to your computer in its ready-to-use form."
        Me.lblExecutableDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grpZipDownload
        '
        Me.grpZipDownload.Controls.Add(Me.lblZipDescription)
        Me.grpZipDownload.Controls.Add(Me.btnDownloadZip)
        Me.grpZipDownload.Location = New System.Drawing.Point(15, 189)
        Me.grpZipDownload.Name = "grpZipDownload"
        Me.grpZipDownload.Size = New System.Drawing.Size(343, 100)
        Me.grpZipDownload.TabIndex = 2
        Me.grpZipDownload.TabStop = False
        '
        'lblZipDescription
        '
        Me.lblZipDescription.Location = New System.Drawing.Point(10, 41)
        Me.lblZipDescription.Name = "lblZipDescription"
        Me.lblZipDescription.Size = New System.Drawing.Size(322, 50)
        Me.lblZipDescription.TabIndex = 1
        Me.lblZipDescription.Text = "This download option requires that you know how to extract files from a .zip (arc" & _
            "hive) file." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "WARNING: Only use this option if the above option doesn't work."
        Me.lblZipDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnDownloadZip
        '
        Me.btnDownloadZip.Location = New System.Drawing.Point(13, 15)
        Me.btnDownloadZip.Name = "btnDownloadZip"
        Me.btnDownloadZip.Size = New System.Drawing.Size(319, 23)
        Me.btnDownloadZip.TabIndex = 0
        Me.btnDownloadZip.Text = "Download As Windows Archive (.zip) File"
        Me.btnDownloadZip.UseVisualStyleBackColor = True
        '
        'btnIgnore
        '
        Me.btnIgnore.Location = New System.Drawing.Point(15, 295)
        Me.btnIgnore.Name = "btnIgnore"
        Me.btnIgnore.Size = New System.Drawing.Size(343, 28)
        Me.btnIgnore.TabIndex = 3
        Me.btnIgnore.Text = "Continue using DNA/RNA Codon Converter v1.2.0"
        Me.btnIgnore.UseVisualStyleBackColor = True
        '
        'webDownloadElement
        '
        Me.webDownloadElement.Location = New System.Drawing.Point(-3, 231)
        Me.webDownloadElement.MinimumSize = New System.Drawing.Size(20, 20)
        Me.webDownloadElement.Name = "webDownloadElement"
        Me.webDownloadElement.Size = New System.Drawing.Size(20, 20)
        Me.webDownloadElement.TabIndex = 4
        Me.webDownloadElement.Visible = False
        '
        'frmUpdate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(370, 346)
        Me.Controls.Add(Me.btnIgnore)
        Me.Controls.Add(Me.grpZipDownload)
        Me.Controls.Add(Me.grpExecutableDownload)
        Me.Controls.Add(Me.lblOutdated)
        Me.MinimumSize = New System.Drawing.Size(378, 373)
        Me.Name = "frmUpdate"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DNA/RNA Codon Converter -- Update your version."
        Me.TopMost = True
        Me.grpExecutableDownload.ResumeLayout(False)
        Me.grpZipDownload.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblOutdated As System.Windows.Forms.Label
    Friend WithEvents grpExecutableDownload As System.Windows.Forms.GroupBox
    Friend WithEvents lblExecutableDescription As System.Windows.Forms.Label
    Friend WithEvents btnDownloadExe As System.Windows.Forms.Button
    Friend WithEvents grpZipDownload As System.Windows.Forms.GroupBox
    Friend WithEvents lblZipDescription As System.Windows.Forms.Label
    Friend WithEvents btnDownloadZip As System.Windows.Forms.Button
    Friend WithEvents btnIgnore As System.Windows.Forms.Button
    Friend WithEvents webDownloadElement As System.Windows.Forms.WebBrowser
End Class
