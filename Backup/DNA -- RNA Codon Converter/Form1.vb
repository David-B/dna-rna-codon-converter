﻿Public Class frmCodonConverter
    Dim updateCheckTracker As Double 'This is used when checking for updates.

    'Upon program start, this checks the program to see if it is the latest version of
    'the program. If it is not, it will trigger an alert for the user.
    Private Sub webUpdate_DocumentCompleted(ByVal sender As Object, ByVal e As System.Windows.Forms.WebBrowserDocumentCompletedEventArgs) Handles webUpdate.DocumentCompleted
        updateCheckTracker += 1
        If updateCheckTracker = 2 Then
            showUpdateAlert()
        End If
    End Sub
    Private Sub webUpdateLink_DocumentCompleted(ByVal sender As Object, ByVal e As System.Windows.Forms.WebBrowserDocumentCompletedEventArgs) Handles webUpdateLink.DocumentCompleted
        updateCheckTracker += 1
        If updateCheckTracker = 2 Then
            showUpdateAlert()
        End If
    End Sub
    Sub showUpdateAlert()
        If webUpdate.Url.ToString = webUpdateLink.Url.ToString Then
            frmUpdate.Show()
        End If
    End Sub



    'This controls the order in which the processing of codon calculations occur.
    Private Sub btnFindCodons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFindCodons.Click
        Dim codonEntryValid As Boolean = True
        If radDnaCodon1.Checked = True Then
            CheckCodonValidity(codonEntryValid, txtDnaCodon1, "DNA1")
            If codonEntryValid = True Then
                FindDNA2()
                FindmRNA()
                FindtRNA()
                FindAminoAcid()
            Else
                MessageBox.Show("Invalid Codon Entry", "Invalid Codon Entry")
            End If
        ElseIf radDnaCodon2.Checked = True Then
            CheckCodonValidity(codonEntryValid, txtDnaCodon2, "DNA2")
            If codonEntryValid = True Then
                FindDNA1()
                FindmRNA()
                FindtRNA()
                FindAminoAcid()
            Else
                MessageBox.Show("Invalid Codon Entry", "Invalid Codon Entry")
            End If
        ElseIf radMrnaCodon.Checked = True Then
            CheckCodonValidity(codonEntryValid, txtMrnaCodon, "mRNA")
            If codonEntryValid = True Then
                mRNAToDNAConverter() 'DNA1 and DNA2 reverse-processing occurs in here.
                FindtRNA()
                FindAminoAcid()
            Else
                MessageBox.Show("Invalid Codon Entry", "Invalid Codon Entry")
            End If
        ElseIf radTrnaCodon.Checked = True Then
            CheckCodonValidity(codonEntryValid, txtTrnaCodon, "tRNA")
            If codonEntryValid = True Then
                FindmRNAFromtRNA()
                mRNAToDNAConverter() 'DNA1 and DNA2 reverse-processing occurs in here.
                FindAminoAcid()
            Else
                MessageBox.Show("Invalid Codon Entry", "Invalid Codon Entry")
            End If
        End If

        If codonEntryValid = True Then
            'Deactivate all radio buttons, and make text boxes read-only. This will
            'be undone when the user clears the program.
            radDnaCodon1.Enabled = False
            radDnaCodon2.Enabled = False
            radMrnaCodon.Enabled = False
            radTrnaCodon.Enabled = False
            radDetermineDnaCodon1.Enabled = False
            radDetermineDnaCodon2.Enabled = False
            txtDnaCodon1.ReadOnly = True
            txtDnaCodon2.ReadOnly = True
            txtMrnaCodon.ReadOnly = True
            txtTrnaCodon.ReadOnly = True
        End If
    End Sub

    'Yes, the above controls the order in which calculations occur, but these
    'are responsible for actually carrying out the calculations.
    Sub FindDNA1()
        Dim hydrogenBaseReader As Integer = 1
        Dim hydrogenBase As Char = txtDnaCodon2.Text.Chars(hydrogenBaseReader - 1)
        While hydrogenBaseReader <> 4
            'Determine hydrogen bases, and find resulting matches.
            If hydrogenBase = "A" Then
                'Found A, concatenate T.
                txtDnaCodon1.Text = txtDnaCodon1.Text & "T"
            ElseIf hydrogenBase = "T" Then
                'Found T, concatenate A.
                txtDnaCodon1.Text = txtDnaCodon1.Text & "A"
            ElseIf hydrogenBase = "C" Then
                'Found C, concatenate G.
                txtDnaCodon1.Text = txtDnaCodon1.Text & "G"
            ElseIf hydrogenBase = "G" Then
                'Found C, concatenate G.
                txtDnaCodon1.Text = txtDnaCodon1.Text & "C"
            End If
            'Now, we add one to the loop counter and substring controls.
            hydrogenBaseReader += 1
            If hydrogenBaseReader <> 4 Then
                hydrogenBase = txtDnaCodon2.Text.Chars(hydrogenBaseReader - 1)
            End If
        End While
    End Sub
    Sub FindDNA2()
        Dim hydrogenBaseReader As Integer = 1
        Dim hydrogenBase As Char = txtDnaCodon1.Text.Chars(hydrogenBaseReader - 1)
        While hydrogenBaseReader <> 4
            'Determine hydrogen bases, and find resulting matches.
            If hydrogenBase = "A" Then
                'Found A, concatenate T.
                txtDnaCodon2.Text = txtDnaCodon2.Text & "T"
            ElseIf hydrogenBase = "T" Then
                'Found T, concatenate A.
                txtDnaCodon2.Text = txtDnaCodon2.Text & "A"
            ElseIf hydrogenBase = "C" Then
                'Found C, concatenate G.
                txtDnaCodon2.Text = txtDnaCodon2.Text & "G"
            ElseIf hydrogenBase = "G" Then
                'Found C, concatenate G.
                txtDnaCodon2.Text = txtDnaCodon2.Text & "C"
            End If
            'Now, we add one to the loop counter and substring controls.
            hydrogenBaseReader += 1
            If hydrogenBaseReader <> 4 Then
                hydrogenBase = txtDnaCodon1.Text.Chars(hydrogenBaseReader - 1)
            End If
        End While
    End Sub
    Sub FindmRNA()
        If radDetermineDnaCodon1.Checked Then
            Dim hydrogenBaseReader As Integer = 1
            Dim hydrogenBase As Char = txtDnaCodon1.Text.Chars(hydrogenBaseReader - 1)
            While hydrogenBaseReader <> 4
                'Determine hydrogen bases, and find resulting matches.
                If hydrogenBase = "A" Then
                    'Found A, concatenate U.
                    txtMrnaCodon.Text = txtMrnaCodon.Text & "U"
                ElseIf hydrogenBase = "T" Then
                    'Found T, concatenate A.
                    txtMrnaCodon.Text = txtMrnaCodon.Text & "A"
                ElseIf hydrogenBase = "C" Then
                    'Found C, concatenate G.
                    txtMrnaCodon.Text = txtMrnaCodon.Text & "G"
                ElseIf hydrogenBase = "G" Then
                    'Found C, concatenate G.
                    txtMrnaCodon.Text = txtMrnaCodon.Text & "C"
                End If
                'Now, we add one to the loop counter and substring controls.
                hydrogenBaseReader += 1
                If hydrogenBaseReader <> 4 Then
                    hydrogenBase = txtDnaCodon1.Text.Chars(hydrogenBaseReader - 1)
                End If
            End While
        ElseIf radDetermineDnaCodon2.Checked Then
            Dim hydrogenBaseReader As Integer = 1
            Dim hydrogenBase As Char = txtDnaCodon2.Text.Chars(hydrogenBaseReader - 1)
            While hydrogenBaseReader <> 4
                'Determine hydrogen bases, and find resulting matches.
                If hydrogenBase = "A" Then
                    'Found A, concatenate U.
                    txtMrnaCodon.Text = txtMrnaCodon.Text & "U"
                ElseIf hydrogenBase = "T" Then
                    'Found T, concatenate A.
                    txtMrnaCodon.Text = txtMrnaCodon.Text & "A"
                ElseIf hydrogenBase = "C" Then
                    'Found C, concatenate G.
                    txtMrnaCodon.Text = txtMrnaCodon.Text & "G"
                ElseIf hydrogenBase = "G" Then
                    'Found C, concatenate G.
                    txtMrnaCodon.Text = txtMrnaCodon.Text & "C"
                End If
                'Now, we add one to the loop counter and substring controls.
                hydrogenBaseReader += 1
                If hydrogenBaseReader <> 4 Then
                    hydrogenBase = txtDnaCodon2.Text.Chars(hydrogenBaseReader - 1)
                End If
            End While
        End If
    End Sub
    Sub FindtRNA()
        Dim hydrogenBaseReader As Integer = 1
        Dim hydrogenBase As Char = txtMrnaCodon.Text.Chars(hydrogenBaseReader - 1)
        While hydrogenBaseReader <> 4
            'Determine hydrogen bases, and find resulting matches.
            If hydrogenBase = "A" Then
                'Found A, concatenate U.
                txtTrnaCodon.Text = txtTrnaCodon.Text & "U"
            ElseIf hydrogenBase = "U" Then
                'Found U, concatenate A.
                txtTrnaCodon.Text = txtTrnaCodon.Text & "A"
            ElseIf hydrogenBase = "C" Then
                'Found C, concatenate G.
                txtTrnaCodon.Text = txtTrnaCodon.Text & "G"
            ElseIf hydrogenBase = "G" Then
                'Found C, concatenate G.
                txtTrnaCodon.Text = txtTrnaCodon.Text & "C"
            End If
            'Now, we add one to the loop counter and substring controls.
            hydrogenBaseReader += 1
            If hydrogenBaseReader <> 4 Then
                hydrogenBase = txtMrnaCodon.Text.Chars(hydrogenBaseReader - 1)
            End If
        End While
    End Sub
    Sub mRNAToDNAConverter()
        If radDetermineDnaCodon1.Checked = True Then
            Dim hydrogenBaseReader As Integer = 1
            Dim hydrogenBase As Char = txtMrnaCodon.Text.Chars(hydrogenBaseReader - 1)
            While hydrogenBaseReader <> 4
                'Determine hydrogen bases, and find resulting matches.
                If hydrogenBase = "A" Then
                    'Found A, concatenate T.
                    txtDnaCodon1.Text = txtDnaCodon1.Text & "T"
                ElseIf hydrogenBase = "U" Then
                    'Found U, concatenate A.
                    txtDnaCodon1.Text = txtDnaCodon1.Text & "A"
                ElseIf hydrogenBase = "C" Then
                    'Found C, concatenate G.
                    txtDnaCodon1.Text = txtDnaCodon1.Text & "G"
                ElseIf hydrogenBase = "G" Then
                    'Found C, concatenate G.
                    txtDnaCodon1.Text = txtDnaCodon1.Text & "C"
                End If
                'Now, we add one to the loop counter and substring controls.
                hydrogenBaseReader += 1
                If hydrogenBaseReader <> 4 Then
                    hydrogenBase = txtMrnaCodon.Text.Chars(hydrogenBaseReader - 1)
                End If
            End While
            FindDNA2()
        ElseIf radDetermineDnaCodon2.Checked = True Then
            Dim hydrogenBaseReader As Integer = 1
            Dim hydrogenBase As Char = txtMrnaCodon.Text.Chars(hydrogenBaseReader - 1)
            While hydrogenBaseReader <> 4
                'Determine hydrogen bases, and find resulting matches.
                If hydrogenBase = "A" Then
                    'Found A, concatenate T.
                    txtDnaCodon2.Text = txtDnaCodon2.Text & "T"
                ElseIf hydrogenBase = "U" Then
                    'Found U, concatenate A.
                    txtDnaCodon2.Text = txtDnaCodon2.Text & "A"
                ElseIf hydrogenBase = "C" Then
                    'Found C, concatenate G.
                    txtDnaCodon2.Text = txtDnaCodon2.Text & "G"
                ElseIf hydrogenBase = "G" Then
                    'Found C, concatenate G.
                    txtDnaCodon2.Text = txtDnaCodon2.Text & "C"
                End If
                'Now, we add one to the loop counter and substring controls.
                hydrogenBaseReader += 1
                If hydrogenBaseReader <> 4 Then
                    hydrogenBase = txtMrnaCodon.Text.Chars(hydrogenBaseReader - 1)
                End If
            End While
            FindDNA1()
        End If
    End Sub
    Sub FindmRNAFromtRNA()
        Dim hydrogenBaseReader As Integer = 1
        Dim hydrogenBase As Char = txtTrnaCodon.Text.Chars(hydrogenBaseReader - 1)
        While hydrogenBaseReader <> 4
            'Determine hydrogen bases, and find resulting matches.
            If hydrogenBase = "A" Then
                'Found A, concatenate U.
                txtMrnaCodon.Text = txtMrnaCodon.Text & "U"
            ElseIf hydrogenBase = "U" Then
                'Found U, concatenate A.
                txtMrnaCodon.Text = txtMrnaCodon.Text & "A"
            ElseIf hydrogenBase = "C" Then
                'Found C, concatenate G.
                txtMrnaCodon.Text = txtMrnaCodon.Text & "G"
            ElseIf hydrogenBase = "G" Then
                'Found C, concatenate G.
                txtMrnaCodon.Text = txtMrnaCodon.Text & "C"
            End If
            'Now, we add one to the loop counter and substring controls.
            hydrogenBaseReader += 1
            If hydrogenBaseReader <> 4 Then
                hydrogenBase = txtTrnaCodon.Text.Chars(hydrogenBaseReader - 1)
            End If
        End While
    End Sub
    Sub FindAminoAcid()
        If txtMrnaCodon.Text = "UUU" Or txtMrnaCodon.Text = "UUC" Then
            txtAminoAcid.Text = "Phenyl-alanine"
        ElseIf txtMrnaCodon.Text = "UUA" Or txtMrnaCodon.Text = "UUG" Or txtMrnaCodon.Text = "CUU" Or txtMrnaCodon.Text = "CUC" Or txtMrnaCodon.Text = "CUA" Or txtMrnaCodon.Text = "CUG" Then
            txtAminoAcid.Text = "Leucine"
        ElseIf txtMrnaCodon.Text = "UCU" Or txtMrnaCodon.Text = "UCC" Or txtMrnaCodon.Text = "UCA" Or txtMrnaCodon.Text = "UCG" Or txtMrnaCodon.Text = "AGU" Or txtMrnaCodon.Text = "AGC" Then
            txtAminoAcid.Text = "Serine"
        ElseIf txtMrnaCodon.Text = "UAU" Or txtMrnaCodon.Text = "UAC" Then
            txtAminoAcid.Text = "Tyrosine"
        ElseIf txtMrnaCodon.Text = "UAA" Or txtMrnaCodon.Text = "UAG" Or txtMrnaCodon.Text = "UGA" Then
            txtAminoAcid.Text = "Stop"
        ElseIf txtMrnaCodon.Text = "UGU" Or txtMrnaCodon.Text = "UGC" Then
            txtAminoAcid.Text = "Cysteine"
        ElseIf txtMrnaCodon.Text = "UGG" Then
            txtAminoAcid.Text = "Tryptophan"
        ElseIf txtMrnaCodon.Text = "CCU" Or txtMrnaCodon.Text = "CCC" Or txtMrnaCodon.Text = "CCA" Or txtMrnaCodon.Text = "CCG" Then
            txtAminoAcid.Text = "Proline"
        ElseIf txtMrnaCodon.Text = "CAU" Or txtMrnaCodon.Text = "CAC" Then
            txtAminoAcid.Text = "Histidine"
        ElseIf txtMrnaCodon.Text = "CAA" Or txtMrnaCodon.Text = "CAG" Then
            txtAminoAcid.Text = "Glutamine"
        ElseIf txtMrnaCodon.Text = "CGU" Or txtMrnaCodon.Text = "CGC" Or txtMrnaCodon.Text = "CGA" Or txtMrnaCodon.Text = "CGG" Or txtMrnaCodon.Text = "AGA" Or txtMrnaCodon.Text = "AGG" Then
            txtAminoAcid.Text = "Arginine"
        ElseIf txtMrnaCodon.Text = "AUU" Or txtMrnaCodon.Text = "AUC" Or txtMrnaCodon.Text = "AUA" Then
            txtAminoAcid.Text = "Isoleucine"
        ElseIf txtMrnaCodon.Text = "AUG" Then
            txtAminoAcid.Text = "Methionine"
        ElseIf txtMrnaCodon.Text = "ACU" Or txtMrnaCodon.Text = "ACC" Or txtMrnaCodon.Text = "ACA" Or txtMrnaCodon.Text = "ACG" Then
            txtAminoAcid.Text = "Threonine"
        ElseIf txtMrnaCodon.Text = "AAU" Or txtMrnaCodon.Text = "AAC" Then
            txtAminoAcid.Text = "Asparagine"
        ElseIf txtMrnaCodon.Text = "AAA" Or txtMrnaCodon.Text = "AAG" Then
            txtAminoAcid.Text = "Lysine"
        ElseIf txtMrnaCodon.Text = "GUU" Or txtMrnaCodon.Text = "GUC" Or txtMrnaCodon.Text = "GUA" Or txtMrnaCodon.Text = "GUG" Then
            txtAminoAcid.Text = "Valine"
        ElseIf txtMrnaCodon.Text = "GCU" Or txtMrnaCodon.Text = "GCC" Or txtMrnaCodon.Text = "GCA" Or txtMrnaCodon.Text = "GCG" Then
            txtAminoAcid.Text = "Alanine"
        ElseIf txtMrnaCodon.Text = "GAU" Or txtMrnaCodon.Text = "GAC" Then
            txtAminoAcid.Text = "Aspartic acid"
        ElseIf txtMrnaCodon.Text = "GAA" Or txtMrnaCodon.Text = "GAG" Then
            txtAminoAcid.Text = "Glutamic acid"
        ElseIf txtMrnaCodon.Text = "GGU" Or txtMrnaCodon.Text = "GGC" Or txtMrnaCodon.Text = "GGA" Or txtMrnaCodon.Text = "GGG" Then
            txtAminoAcid.Text = "Glycine"
        End If
    End Sub

    'These are all responsible for changing which codon the user is editing. The user
    'selects which codon he wants to edit by using the corresponding radio buttons.
    Private Sub radDnaCodon1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radDnaCodon1.CheckedChanged
        txtDnaCodon1.ReadOnly = False
        txtDnaCodon2.ReadOnly = True
        txtMrnaCodon.ReadOnly = True
        txtTrnaCodon.ReadOnly = True
        txtDnaCodon1.Focus()
    End Sub
    Private Sub radDnaCodon2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radDnaCodon2.CheckedChanged
        txtDnaCodon1.ReadOnly = True
        txtDnaCodon2.ReadOnly = False
        txtMrnaCodon.ReadOnly = True
        txtTrnaCodon.ReadOnly = True
        txtDnaCodon2.Focus()
    End Sub
    Private Sub radMrnaCodon_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radMrnaCodon.CheckedChanged
        txtDnaCodon1.ReadOnly = True
        txtDnaCodon2.ReadOnly = True
        txtMrnaCodon.ReadOnly = False
        txtTrnaCodon.ReadOnly = True
        txtMrnaCodon.Focus()
    End Sub
    Private Sub radTrnaCodon_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radTrnaCodon.CheckedChanged
        txtDnaCodon1.ReadOnly = True
        txtDnaCodon2.ReadOnly = True
        txtMrnaCodon.ReadOnly = True
        txtTrnaCodon.ReadOnly = False
        txtTrnaCodon.Focus()
    End Sub

    'These will convert all text entered by the user in any of the textboxes to
    'uppercase so that the codons appear as they should to the user. They will also
    'make sure that no more than three characters are entered in the textbox at a time.
    Private Sub txtDnaCodon1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDnaCodon1.TextChanged
        UppercaseAndCharacterControl(txtDnaCodon1)
    End Sub
    Private Sub txtDnaCodon2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDnaCodon2.TextChanged
        UppercaseAndCharacterControl(txtDnaCodon2)
    End Sub
    Private Sub txtMrnaCodon_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMrnaCodon.TextChanged
        UppercaseAndCharacterControl(txtMrnaCodon)
    End Sub
    Private Sub txtTrnaCodon_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTrnaCodon.TextChanged
        UppercaseAndCharacterControl(txtTrnaCodon)
    End Sub
    'This is the master. All of the others call this to do the dirty work for them.
    Sub UppercaseAndCharacterControl(ByVal textbox As TextBox)
        Dim cursorLocation As Integer = textbox.SelectionStart 'Determine the cursor's location in the textbox, store it.
        textbox.Text = textbox.Text.ToUpper 'Convert the text in the box to uppercase.
        textbox.Select(cursorLocation, 0) 'Set the location of the cursor based on what was previously stored.

        'Check to see if the textbox already has three characters in it. If it does,
        'then let's delete everything except the last 3 characters.
        If textbox.Text.Length > 3 Then
            textbox.Text = textbox.Text.ToUpper.Substring(0, 3)
        End If
    End Sub

    'This clears the textboxes of all codon entries.
    Private Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtDnaCodon1.Clear()
        txtDnaCodon2.Clear()
        txtMrnaCodon.Clear()
        txtTrnaCodon.Clear()
        txtAminoAcid.Clear()

        'Activate all radio buttons, and use the currently selected radio button to
        'determine which textbox to make editable. This needs to be done because all
        'text-boxes were made read-only and all radio buttons were disabled when the
        'user clicked the codon finder button.
        radDnaCodon1.Enabled = True
        radDnaCodon2.Enabled = True
        radMrnaCodon.Enabled = True
        radTrnaCodon.Enabled = True
        radDetermineDnaCodon1.Enabled = True
        radDetermineDnaCodon2.Enabled = True

        If radDnaCodon1.Checked = True Then
            txtDnaCodon1.ReadOnly = False
            txtDnaCodon1.Focus()
        ElseIf radDnaCodon2.Checked = True Then
            txtDnaCodon2.ReadOnly = False
            txtDnaCodon2.Focus()
        ElseIf radMrnaCodon.Checked = True Then
            txtMrnaCodon.ReadOnly = False
            txtMrnaCodon.Focus()
        ElseIf radTrnaCodon.Checked = True Then
            txtTrnaCodon.ReadOnly = False
            txtTrnaCodon.Focus()
        End If
    End Sub

    'This shows the changelog form.
    Private Sub btnAbout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAbout.Click
        AboutBox1.Show()
    End Sub

    'This will check the codon provided by the user for validity.
    Sub CheckCodonValidity(ByRef CodonValid As Boolean, ByVal txtCodon As TextBox, ByRef type As String)
        Dim hydrogenBaseCounter As Integer = 1
        Dim hydrogenBase As Char = txtCodon.Text.Chars(hydrogenBaseCounter - 1)
        While hydrogenBaseCounter <> 4
            If type = "DNA1" Or type = "DNA2" Then
                If hydrogenBase <> "A" And hydrogenBase <> "T" And hydrogenBase <> "G" And hydrogenBase <> "C" Then
                    CodonValid = False
                End If
            Else
                If hydrogenBase <> "A" And hydrogenBase <> "U" And hydrogenBase <> "G" And hydrogenBase <> "C" Then
                    CodonValid = False
                End If
            End If
            hydrogenBaseCounter += 1
            If hydrogenBaseCounter <> 4 Then
                hydrogenBase = txtCodon.Text.Chars(hydrogenBaseCounter - 1)
            End If
        End While
    End Sub
End Class
